const common = require('./webpack.common')
const { merge } = require('webpack-merge');
const miniCssExtractPlugin = require('mini-css-extract-plugin')
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const TerserJSPPlugin = require('terser-webpack-plugin')
module.exports = merge(common, {
    mode:"production",
    module: {
        rules: [
            {
                test: /\.(css|scss)$/i, 
                use: [{
                    loader: miniCssExtractPlugin.loader,
                    options: {
                        publicPath: ''
                    }
                },'css-loader','postcss-loader','sass-loader']
            },

        ]
    },
    plugins: [
        new miniCssExtractPlugin({
            filename: 'css/[name]-[contenthash].css'
        })
    ], 
    optimization: {
        minimizer: [ new TerserJSPPlugin({parallel: true}), new OptimizeCssAssetsPlugin()],
        splitChunks: {
            chunks: 'all'
        }
    }
})