const path = require('path')
const htmlWebpackPlugin = require('html-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = {
    entry: {
        "index":"./src/index.js",
        "video": './src/video.js'
    }, 
    output: {
        filename : "js/[name]-[contenthash].js",
        path: path.resolve(__dirname, 'dist')
    },
    module: {
        rules: [
            {
                test: /\.webm$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name]-[contenthash].[ext]',
                            outputPath: 'medias'
                        }
                    }
                ]
            },
            {
                test: /\.(jpg|jpeg|gif|png)$/i,
                use: [
                {
                    loader:'url-loader',
                        options: {
                            limit: 63000,
                            name: '[name]-[contenthash].[ext]',
                            outputPath:'imagestxt'
                        }
                }]
            },
            {
                test: /\.(woff(2)?|ttf|eot|svg)$/,
                use: [{
                    loader: 'file-loader',
                    options:{
                        name:'[name]-[contenthash].[ext]',
                        outputPath:'fonts'

                    }
                }]
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: [{
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }]
            }
        ],
    },
    plugins: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            template: 'src/templates/index.html',
            filename: 'index.html',
            title: 'Bienvenue sur mon site',
            chunks:['index']
        }),
        new HtmlWebpackPlugin({
            template: 'src/templates/video.html',
            filename: 'video.html',
            title: 'ma video',
            chunks:['video']

        })
    ]
}