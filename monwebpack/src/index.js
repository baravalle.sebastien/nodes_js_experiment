import $ from 'jquery'
import 'bootstrap'
import './styles/styles.scss'
import videojs from 'video.js'
$('#title').html('Bonjour Webpack5 !')
import moi from './media/video.webm'

videojs(document.querySelector('#mavideo'), {
    control: true
}).ready( ()=>{
    let vPlayer = this
    vPlayer.src(moi)
})
