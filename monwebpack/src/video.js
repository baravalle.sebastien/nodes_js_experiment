import 'bootstrap'
import './styles/styles.scss'
import videojs from 'video.js'
import moi from './media/video.webm'

videojs(document.querySelector('#mavideo'), {
    controls: true,
    loop: true,
    muted: true,
    autoplay: true,

}).ready( function(){
    let vPlayer = this
    vPlayer.src(moi)
   setTimeout(function() {
    vPlayer.playbackRate(12);
},0);
})
