const common = require('./webpack.common')
const { merge } = require('webpack-merge');
const path = require('path')

module.exports = merge(common, {
    mode:"development",
    module: {
        rules: [
            {
                test: /\.(css|scss)$/i, 
                use: ['style-loader','css-loader','postcss-loader','sass-loader']
            },

        ]
    },
    devServer: {
        port: 3001,
        contentBase: path.join(__dirname, 'dist'),
        watchOptions: {
            ignored: /node_modules/
        },
    }
})